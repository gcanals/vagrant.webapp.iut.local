host : 192.168.56.12
hostname : webapp.local

ubuntu1404-x64 / php 5

Répertoire synchronisé : ./ --> /var/www

apache :
  - vhost défaut : webapp.local
           docroot : /var/www/html (-> ./html)

  - vhost : mywebapp.local www.mywebapp.local
          docroot :  /var/www/ (-> ./)

php : 5.5 - config dev / xdebug
ruby + python
mysql : 5.5 - root/123
accès : webapp.local/adminer

mongo installé avec php_mongo
redis installé avec php_redis
sqlite installé avec php_sqlite
